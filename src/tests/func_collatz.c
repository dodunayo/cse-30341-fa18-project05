/* func_collatz.c: compute collatz */

#include "memhashed/cache.h"
#include "memhashed/queue.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define SENTINEL (-1)

/* Globals */

Cache  *Collatz = NULL;
Queue  *Numbers = NULL;

/* Threads */

/**
 * Continuously computes the collatz length for the data in the Numbers Queue
 * until the SENTINEL is encountered.
 */
void *	collatz_thread(void *arg) {
    // TODO: Implement worker thread
	uint64_t queueValue = queue_pop(Numbers);
	while(queueValue != SENTINEL)
	{
		printf("Collatz(%lu) = %ld\n", queueValue, cache_get(Collatz, queueValue));
		queueValue = queue_pop(Numbers);
	}
    return NULL;
}

/* Handler */

/**
 * Recursively computes collatz length for specified key.
 * @param   key	    Number to computer length for.
 * @return  Length of collatz sequence for specified key.
 */
int64_t	collatz_handler(const uint64_t key) {
    // TODO: Implement handler
	if (key == 1)
	{
		return 1;
	}

	if(key % 2 == 0)
	{
		return cache_get(Collatz, key/2) + 1;
	}else
	{
		return cache_get(Collatz, 3*key + 1) + 1;
	}
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    // TODO: Parse command line arguments
	size_t addrLen    = strtol(argv[1], NULL, 10);
	size_t pageSize   = strtol(argv[2], NULL, 10);
	Policy policy     = strtol(argv[3], NULL, 10);
	size_t numThreads = strtol(argv[4], NULL, 10);
	
    // TODO: Create Collatz Cache
	Collatz = cache_create(addrLen, pageSize, policy, collatz_handler);
    // TODO: Create Numbers Queue
	Numbers = queue_create(SENTINEL, BUFSIZ);
    
	// TODO: Create Worker threads
	Thread threads[numThreads];
	for(int i = 0; i < numThreads; i++)
	{
		thread_create(&threads[i], NULL, collatz_thread, NULL);
	}
    // TODO: Read numbers from standard input and add to Numbers queue
	int64_t numInput = 0;
	while (scanf("%li", &numInput) != EOF)
	{
		queue_push(Numbers, numInput);
	}

	queue_push(Numbers, -1);

    // TODO: Join Worker threads
	for (int k = 0; k < numThreads; k++)
	{
		thread_join(threads[k], NULL);
	}

    // TODO: Output Collatz Cache statistics
    cache_stats(Collatz, stdout);
    // TODO: Delete Collatz Cache and Numbers Queue
	cache_delete(Collatz);
	queue_delete(Numbers);
    return EXIT_SUCCESS;
}
