/* cache.c: MemHashed Cache structure */

#include "memhashed/cache.h"

/* Cache Functions */

/**
 * Create Cache Structure.
 * @param   addrlen     Length of virtual address.
 * @param   page_size   Number of entries per page.
 * @param   policy      Which eviction policy to use.
 * @param   handler     Handler function to call on cache misses.
 * @return  Newly allocated Cache Structure with allocated pages.
 */
Cache *	cache_create(size_t addrlen, size_t page_size, Policy policy, Handler handler) {
    // TODO: Allocate Cache Structure and configure internal fields.
    Cache *c = (Cache *)malloc(sizeof(Cache));
    mutex_init(&c->lock, NULL); // Should be in the struct
    mutex_lock(&c->lock);
    
    c->addrlen   = addrlen;
    c->handler   = handler;
    c->hits      = 0;
    c->page_size = page_size;
    c->policy    = policy;
    c->misses    = 0;
    c->naddresses = 1;

    for (int i = 0; i < addrlen; i++)
    {
        c->naddresses *=2;
    }
    if (addrlen == 0)
    {
        c->naddresses = 0;
    }

    if ((addrlen > 0) && (page_size > 0))
    {
        c->npages = c->naddresses / page_size;
    }else{
        c->npages = 0;
    }
    uint64_t inc = 1;
    size_t vpnBits =0;

    while(inc < c->npages)
    {
        vpnBits++;
        inc *= 2;
    }

    c->vpn_shift = addrlen - vpnBits;
    c->vpn_mask  = 0;
    for (int j = 0; j < vpnBits; j++)
    {
        c->vpn_mask = c->vpn_mask << 1;
        c->vpn_mask++;
    }
    
    c->vpn_mask = c->vpn_mask << c->vpn_shift;
    c->offset_mask = 0;

    for (int k = 0; k < c->vpn_shift; k++)
    {
        c->offset_mask = c->offset_mask << 1;
        c->offset_mask++;
    }
    
    if(c->npages > 0)
    {
        c->pages = (Page**)malloc(c->npages*sizeof(Page*));
    }else{
        c->pages = NULL;
    }

    for(int l = 0; l < c->npages; l++)
    {
        c->pages[l] = page_create(page_size, policy);
    }
    mutex_unlock(&(c->lock));
    return c;
}

/**
 * Delete Cache Structure (including internal pages).
 * @param   cache       Pointer to Cache Structure.
 */
void    cache_delete(Cache *cache) {
    // TODO: Deallocate Cache Structure and internal fields.
    mutex_lock(&(cache->lock));
    for (int i = 0; i < cache->npages; i++)
    {
        page_delete(cache->pages[i]);
    }
    free(cache->pages);
    mutex_unlock(&cache->lock);
    pthread_mutex_destroy(&cache->lock);
    free(cache);
}

/**
 * Retrieves value for specified for key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to lookup appropriate page:
 *
 *      a. If entry is found, then return value.
 *
 *      b. Otherwise, use the handler to generate the value for the
 *      corresponding key.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @return  value for the correspending key.
 */
int64_t	cache_get(Cache *cache, const uint64_t key) {
    mutex_lock(&cache->lock);
    uint64_t vAddr  = key % cache->naddresses;
    uint64_t vpn    = (vAddr & cache->vpn_mask) >> cache->vpn_shift;
    uint64_t offset = vAddr & cache->offset_mask;

    Page *p = cache->pages[vpn];
    Entry e = page_get(p, key, offset);

    if(e.key != key)
    {
        cache->misses++; 
        mutex_unlock(&cache->lock);
        uint64_t cacheValue = cache->handler(key);
        cache_put(cache, key, cacheValue);
        return cacheValue;
    }else{
        cache->hits++;
        mutex_unlock(&cache->lock);
        return e.value;
    }
}

/**
 * Insert or update value for specified key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to insert or update value into appropriate page.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   value       Value to update or replace old value with.
 */
void	cache_put(Cache *cache, const uint64_t key, const int64_t value) {
    mutex_lock(&cache->lock);
    uint64_t vAddr  = key % cache->naddresses;
    uint64_t vpn    = (vAddr & cache->vpn_mask) >> cache->vpn_shift;
    uint64_t offset = vAddr & cache->offset_mask;

    Page * p = cache->pages[vpn];
    page_put(p, key, value, offset);
    mutex_unlock(&cache->lock);
}

/**
 * Display cache hits and misses to specified streams.
 * @param   cache       Pointer to Cache Structure.
 * @param   stream      File stream to write to.
 */
void	cache_stats(Cache *cache, FILE *stream) {
    mutex_lock(&cache->lock);
    fprintf(stream, "Hits   = %lu\n", cache->hits);
    fprintf(stream, "Misses = %lu\n", cache->misses);
    mutex_unlock(&cache->lock);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
