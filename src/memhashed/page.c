/* page.c: MemHashed Page structure */

#include "memhashed/page.h"

/* Prototypes */

size_t  page_evict_fifo(Page *page, size_t offset);
size_t  page_evict_random(Page *page, size_t offset);
size_t  page_evict_lru(Page *page, size_t offset);
size_t  page_evict_clock(Page *page, size_t offset);

/* Functions */

/**
 * Create Page Structure.
 * @param   nentries    Number of entries in page.
 * @param   policy      Which eviction policy to use.
 * @return  Newly allocated Page Structure with allocated entries.
 */
Page *	page_create(size_t nentries, Policy policy) {
    // TODO: Allocate Page Structure and internal fields.
    Page *p = (Page*)malloc(sizeof(Page));

    mutex_init(&p->lock, NULL);
    mutex_lock(&p->lock);
    p->entries  = (Entry *)malloc(nentries*sizeof(Entry));
    p->nentries = nentries;
    p->policy   = policy;
    
    for(int i = 0; i < nentries; i++)
    {
        p->entries[i].valid = false;
    }

    p->clock_hand = 0;
    p->order      = 0;
    p->used       = 0;
    mutex_unlock(&p->lock);
    return p;
}

/**
 * Delete Page Structure (including internal entries).
 * @param   page        Pointer to Page Structure.
 */
void    page_delete(Page *page) {
    mutex_lock(&page->lock);
    free(page->entries);
    mutex_unlock(&page->lock);
    pthread_mutex_destroy(&page->lock);
    free(page);
        
        // TODO: Deallocate Page Structure and internal fields.
}

/**
 * Searches Page for Entry with specified key, beginning at specified offset.
 *
 *  This function performs a linear probe of the entries array beginning with
 *  the specified offset until a matching key is found or all entries have been
 *  probed.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   offset      Initial offset to begin probe.
 * @return  Entry with corresponding key or an Entry with a mismatched key if not found.
 */
Entry   page_get(Page *page, const uint64_t key, size_t offset) {
    mutex_lock(&page->lock);
    if(offset >= page->nentries)
    {
        Entry invalidE;
        invalidE.valid = false;
        invalidE.key = key+1;
        mutex_unlock(&page->lock);
        return invalidE;
    }
    for(int i = 0; i < page->nentries; i++)
    {
        int pos = (i + offset) % page->nentries;
        if (page->entries[pos].key == key && page->entries[pos].valid)
        {
            page->entries[pos].used = ++page->used;
            mutex_unlock(&page->lock);
            return page->entries[pos];
        }
    }
    Entry invalidE;
    invalidE.valid = false;
    invalidE.key = key + 1;
    mutex_unlock(&page->lock);
    return invalidE;
}

/**
 * Insert or update Entry with specified key with the new value.
 *
 *  This function begins a linear probe for the Entry with the corresponding
 *  key:
 *
 *      1. If it is found, then the Entry is updated.
 *
 *      2. If it is not found, then the first invalid Entry is used to insert a
 *      new key and value.
 *
 *      3. If it is not found and there are no invalid Entries, then one is
 *      evicted based on the previously specified Policy and then used
 *      to insert a new key and value.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @parm    value       Value to update or replace old value with.
 * @param   offset      Initial offset to begin probe.
 */
void    page_put(Page *page, const uint64_t key, const int64_t value, size_t offset) {
    mutex_lock(&page->lock);
    Entry *e = NULL;
    Entry *invalidPage = NULL;

    for (int i = 0; i < page->nentries; i++)
    {
        int pos = (i+ offset) % page->nentries;
        if (!page->entries[pos].valid && !invalidPage)
        {
            invalidPage = &(page->entries[pos]);
        }
        if(page->entries[pos].key == key && page->entries[pos].valid)
        {
            e = &(page->entries[pos]);
            e->value = value;
            e->valid = true;
            e->used  = ++page->used;
            mutex_unlock(&page->lock);
            return;
        }
    }
    if(invalidPage)
    {
        invalidPage->key = key;
        invalidPage->used = ++page->used;
        invalidPage->order = ++page->order;
        invalidPage->valid = true;
        invalidPage->value = value;
        mutex_unlock(&page->lock);
        return;
    }else{
        size_t eOffset = offset;
        switch (page->policy)
        {
            case FIFO:
                eOffset = page_evict_fifo(page, offset);
            break;
            case CLOCK:
                eOffset = page_evict_clock(page, offset);
            break;
            case LRU:
                eOffset = page_evict_lru(page, offset);
            break;
            case RANDOM:
                eOffset = page_evict_random(page, offset);
            break;
        }
        page->entries[eOffset].valid = false;
        mutex_unlock(&page->lock);
        page_put(page, key, value, offset);
        return;
    }
}

/**
 * Select Entry to evict based on FIFO strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_fifo(Page *page, size_t offset) {
    size_t mOrder = page->entries[offset].order;
    int minPos = offset;

    for (int i = 0; i < page->nentries; i++)
    {
        int pos = (i + offset) % page->nentries;
        if (page->entries[pos].order < mOrder)
        {
            mOrder = page->entries[pos].order;
            minPos = pos;
        }
    }
    return minPos;
}

/**
 * Select Entry to evict based on Random strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_random(Page *page, size_t offset) {
    return rand() % page->nentries;
}

/**
 * Select Entry to evict based on LRU strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_lru(Page *page, size_t offset) {
    size_t minValue = page->entries[offset].used;
    int minPos = offset;
    for(int i = 0; i < page->nentries; i++)
    {
        int pos = (i + offset) % page->nentries;
        if(page->entries[pos].used < minValue) 
        {
            minValue = page->entries[pos].used;
            minPos  = pos;
        }
    }
    return minPos;
}

/**
 * Select Entry to evict based on Clock strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
//TODO: Fix function below
size_t  page_evict_clock(Page *page, size_t offset) {
    while(page->entries[page->clock_hand].used != 0)
    {
        page->entries[page->clock_hand].used = 0;
        page->clock_hand = (page->clock_hand + 1) % page->nentries;
    }
    return page->clock_hand;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
